import numpy as np
import os, pickle

import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
from keras.layers import Dense, Dropout
import keras
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from sklearn import svm

import sklearn
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
from joblib import dump, load

with open("model_data/data.pkl", "rb") as mypicklefile:
    data = pickle.load(mypicklefile)

with open("model_data/labels.pkl", "rb") as mypicklefile:
    labels = pickle.load(mypicklefile)

accs = []

for bit in range(len(labels[0])):
    
    new_labels = np.array([int(i[bit]) for i in labels])


    X_train, X_test, y_train, y_test = train_test_split(
        data, 
        new_labels, 
        test_size=0.25, 
        random_state=15378586,
        shuffle = True
    )


    model = Sequential()


    model.add(Dense(400, input_shape=(507,), activation='relu'))
    model.add(Dense(320, activation='relu'))
    model.add(Dense(300, activation='relu'))
    model.add(Dense(264, activation='relu'))
    model.add(Dense(230, activation='relu'))
    model.add(Dropout(0.7))

    model.add(Dense(1, activation='sigmoid', name='output'))


    model.compile(optimizer=keras.optimizers.Adam(lr=1e-4), loss='binary_crossentropy', metrics=['accuracy'])

    model.fit(X_train, y_train, epochs=15)

    net_loss, net_acc = model.evaluate(X_test, y_test)

    
    
    # dic = {}
    # for __ in model.predict_classes(X_test):
    #     if __[0] in dic:
    #         dic[__[0]] += 1
    #     else:
    #         dic[__[0]] = 1
    # print(dic)
    print(net_acc)
    accs.append(net_acc)

    # rf_clf = RandomForestClassifier(n_estimators=3000, max_depth=30, random_state=15378586)
    # rf_clf.fit(X_train, y_train)
    # conf = confusion_matrix(y_test, rf_clf.predict(X_test))
    # rf_acc = (conf[0][0] + conf[1][1]) / (conf[0][0] + conf[1][1] + conf[0][1] + conf[1][0])
    # print(conf)
    # print(rf_acc)

    # svm_clf = svm.SVC()
    # svm_clf.fit(X_train, y_train)
    # conf = confusion_matrix(y_test, svm_clf.predict(X_test))
    # svm_acc = (conf[0][0] + conf[1][1]) / (conf[0][0] + conf[1][1] + conf[0][1] + conf[1][0])
    # print(conf)
    # print(svm_acc)

    # if svm_acc > rf_acc and svm_acc > net_acc:
    #     dump(svm_clf, 'models/svm_{bit}_bit_model.joblib') 
    # elif rf_acc > svm_acc and rf_acc > net_acc:
    #     dump(rf_clf, 'models/rf_{bit}_bit_model.joblib') 
    # else:
        # model.save("models/net_{bit}_bit_model.h5".format(bit=bit))
    
    model.save("models/net_{bit}_bit_model.h5".format(bit=bit))


    keras.backend.clear_session()

print(np.mean(accs))
