import pickle, numpy as np


edges_data = {}

with open("select_data/cifar100_images_gray.pkl", "rb") as mypicklefile:
    cifar100_images = pickle.load(mypicklefile)
    for i in cifar100_images.keys():
        cifar100_images[i] = np.array(cifar100_images[i])
    edges_data.update(cifar100_images)

with open("select_data/fashion_mnist_images_gray.pkl", "rb") as mypicklefile:
    fashion_mnist_images = pickle.load(mypicklefile)
    for i in fashion_mnist_images.keys():
        fashion_mnist_images[i] = np.array(fashion_mnist_images[i])
    edges_data.update(fashion_mnist_images)

with open("select_data/mnist_images_gray.pkl", "rb") as mypicklefile:
    mnist_images = pickle.load(mypicklefile)
    for i in mnist_images.keys():
        mnist_images[i] = np.array(mnist_images[i])
    edges_data.update(mnist_images)

with open("select_data/cifar10_images_gray.pkl", "rb") as mypicklefile:
    cifar10_images = pickle.load(mypicklefile)
    for i in cifar10_images.keys():
        cifar10_images[i] = np.array(cifar10_images[i])
    edges_data.update(cifar10_images)

with open("transform_data/network_arch.pkl", "rb") as mypicklefile:
    net_data = pickle.load(mypicklefile)

count = 0

for i in net_data:
    labels = i['labels']
    
    current_edges = edges_data[labels[0]]
    arr = np.zeros((current_edges.shape[1], current_edges.shape[2]))
    total_images = len(edges_data[labels[0]])
    for j in current_edges:
        arr += j

    for label in range(1, len(labels)):
        for j in edges_data[labels[label]]:
            arr += j

        total_images += len(edges_data[labels[label]])
    
    i['edges'] =  arr / total_images
    if count % 100 == 0:
        print(count)
    count += 1 
    # break
   

# d = []
# for i in net_data:
#     if "edges" in i:
#         d.append(i)


with open("transform_data/network_arch.pkl", "wb") as mypicklefile:
    pickle.dump(net_data, mypicklefile)













# count = 0
# for i in net_data:
#     count += 1
#     if count % 100 == 0:
#         print(count)
#     # print(i['labels'])
#     # merged_data = filters.sobel(list(dataset_data[i['labels'][0]]))
#     # print(merged_data)
#     # for label in range(1, len(i['labels'])):
#     #     merged_data += filters.sobel(dataset_data[i['labels'][label]])
#     # break
    


# with open("transform_data/network_arch.pkl", "wb") as mypicklefile:
#     pickle.dump(net_data, mypicklefile)


# with open("select_data/cifar100_images_gray.pkl", "rb") as mypicklefile:
#     data = pickle.load(mypicklefile)

# current_data = list(data['apples'])

# for i in data['baby']:
#     current_data.append(i)

# merged_data = filters.sobel(current_data[0])
# for i in range(1, len(current_data)):
#     merged_data += filters.sobel(current_data[i])

# print(merged_data / len(current_data))



# # print(merged_data.shape)
# # edges = filters.sobel(merged_data)
# # print('sobel', edges)
# # print(len(edges))
# # print(feature.canny(merged_data))

