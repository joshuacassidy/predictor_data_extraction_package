import cv2, os, pickle


def pickle_dataset(dataset):
    dataset_location = "{dataset}/train".format(dataset=dataset)

    image_cat = os.listdir(dataset_location)

    dataset_loaded = {}

    for cat in image_cat:
        images = []
        if cat.startswith('.'):
            continue
        for i in os.listdir(os.path.join(dataset_location, cat)):
            if i.startswith('.'):
                continue
            image = cv2.imread(os.path.join(dataset_location, cat, i))
            images.append(image)
        dataset_loaded[cat] = images

    with open("select_data/{dataset}_images.pkl".format(dataset=dataset), "wb") as mypicklefile:
        pickle.dump(dataset_loaded, mypicklefile)

pickle_dataset('cifar100')
pickle_dataset('cifar10')
pickle_dataset('fashion_mnist')
pickle_dataset('mnist')

def pickle_dataset_gray(dataset):
    dataset_location = "{dataset}/train".format(dataset=dataset)

    image_cat = os.listdir(dataset_location)

    dataset_loaded = {}

    for cat in image_cat:
        images = []
        if cat.startswith('.'):
            continue
        for i in os.listdir(os.path.join(dataset_location, cat)):
            if i.startswith('.'):
                continue
            image = cv2.imread(os.path.join(dataset_location, cat, i), cv2.IMREAD_GRAYSCALE)
            images.append(image)
        dataset_loaded[cat] = images

    with open("select_data/{dataset}_images_gray.pkl".format(dataset=dataset), "wb") as mypicklefile:
        pickle.dump(dataset_loaded, mypicklefile)

pickle_dataset_gray('cifar100')
pickle_dataset_gray('cifar10')
pickle_dataset_gray('fashion_mnist')
pickle_dataset_gray('mnist')

