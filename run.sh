#!/bin/bash

python3 extract_images_data/extract_cifar10.py
python3 extract_images_data/extract_cifar100.py
python3 extract_images_data/fashion_mnist.py
python3 extract_images_data/mnist_extract.py

python3 selection/selection.py
python3 selection/select_images.py

python3 transform/serialise_arch.py
python3 transform/transform_color.py

python3 transform/transform_edges.py

python3 transform/add_edges_arch.py
python3 transform/zip_data.py

python3 transform/transform_training.py

python3 transform/transform_resize_data.py

python3 data_mining/run_each_bit.py
